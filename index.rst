

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://gdevops.frama.io/python/news-2023/rss.xml>`_

.. _python_2023:

==========================
**Python** 2023
==========================


.. toctree::
   :maxdepth: 3

   12/12
   11/11
   10/10
   08/08
   07/07
   06/06
   05/05
   04/04
   03/03
   02/02
   01/01
