.. index::
   pair: Bloated environment; cresote
   ! creosote


.. _decouverte_creosote:

================================================================================================
2023-03-12 **creosote: Identify unused dependencies and avoid a bloated virtual environment**
================================================================================================

- https://github.com/fredrikaverpil/creosote


Description
===============

Identify unused dependencies and avoid a bloated virtual environment.


History and ambition
========================

The idea of a package like this was born from having gotten security
vulnerability reports about production dependencies (shipped into production)
which turned out to not not even be in use.

The goal would be to be able to run this tool in CI, which will catch
cases where the developer forgets to remove unused dependencies.

An example of such a case could be when doing refactorings.

Note: The creosote tool supports identifying both unused production
dependencies and developer dependencies.

It all depends on what you would like to achieve.
