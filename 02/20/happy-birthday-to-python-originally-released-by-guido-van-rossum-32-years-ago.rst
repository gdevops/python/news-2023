

=============================================================================================================================
2023-02-20 Happy Birthday to Python, originally released by Guido van Rossum 32 years ago on this day (February 20, 1991)
=============================================================================================================================

- https://fosstodon.org/@paulox/109899532864216587

Happy Birthday to Python, originally released by Guido van Rossum 32 years ago on this day (February 20, 1991) on the usenet alt.sources 🥳

"This is Python, an extensible interpreted programming language that
combines remarkable power with very clear syntax.

This is version 0.9 (the first beta release), patchlevel 1." 🐍

The archived message: 👇

- https://web.archive.org/web/20210811171015/https://www.tuhs.org/Usenet/alt.sources/1991-February/001749.html

Photo credit: Copyright 1999 Guido van Rossum ( https://gvanrossum.github.io/pics.html )

CC @ThePSF

#Python #GuidoVanRossum #Usenet
