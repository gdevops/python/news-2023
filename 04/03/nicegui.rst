.. index::
   pair: nicegui; 2023-04-03
   pair: GUI; nicegui
   ! nicegui

.. _nicegui_2023_04_03:

==================================
2023-04-03 **nicegui**
==================================

- https://github.com/zauberzeug/nicegui/
- https://nicegui.io/
- https://nicegui.io/documentation


Description
=============

Interact with Python through buttons, dialogs, 3D scenes, plots and much more.

NiceGUI handles all the web development details for you.

So you can focus on writing Python code.

Anything from short scripts and dashboards to full robotics projects,
IoT solutions, smart home automations and **machine learning** projects can
benefit from having all code in one place.

API responses
==============

- https://nicegui.io/documentation#api_responses

NiceGUI is based on FastAPI.

This means you can use all of FastAPI's features. For example, you can
implement a RESTful API in addition to your graphical user interface.


Deployment
===========

- https://nicegui.io/documentation#server_hosting

To deploy your NiceGUI app on a server, you will need to execute your
main.py (or whichever file contains your ui.run(...)) on your cloud
infrastructure.

You can, for example, just install the NiceGUI python package via pip
and use systemd or similar service to start the main script.

In most cases, you will set the port to 80 (or 443 if you want to use
HTTPS) with the ui.run command to make it easily accessible from the outside.
