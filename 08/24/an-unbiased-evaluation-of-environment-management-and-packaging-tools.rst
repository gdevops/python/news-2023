.. index::
   pair: Anna-Lena Popkes; An unbiased evaluation of environment management and packaging tools (2023-08-24)

.. _popkes_2023_08_24:

===============================================================================================================
2023-08-24 **An unbiased evaluation of environment management and packaging tools** by Anna-Lena Popkes
===============================================================================================================

- https://alpopkes.com/posts/python/packaging_tools/
- https://talkpython.fm/episodes/show/436/an-unbiased-evaluation-of-environment-and-packaging-tools (2023-11-01)
- :ref:`tuto_python:python_packaging`

Author : Anna-Lena Popkes
=============================

- https://alpopkes.com/
- https://github.com/zotroneneis
- https://alpopkes.com/posts/
- https://github.com/zotroneneis/magical_universe

Motivation
==================

- https://www.youtube.com/watch?v=MsJjzVIVs6M

When I started with Python and created my first package I was confused. 

Creating and managing a package seemed much harder than I expected. 

In addition, multiple tools existed and I wasn’t sure which one to use. 

I’m sure most of you had the very same problem in the past. Python has a 
zillion tools to manage virtual environments and create packages and it 
can be hard (or almost impossible) to understand which one fits your needs. 

Several talks and blog post on the topic exist, but none of them gives a 
complete overview or evaluates the tools in a structured fashion. 

This is what this post is about. I want to give you a truly unbiased evaluation 
of existing packaging and environment management tools. 

In case you’d rather watch a talk, take a look at the recording of `PyCon DE 2023 <https://www.youtube.com/watch?v=MsJjzVIVs6M>_ 
or `EuroPython 2023 <https://www.youtube.com/watch?v=3-drZY3u5vo>`_.

.. youtube:: 3-drZY3u5vo


Categorization
===================

For the purpose of this article I identified five main categories that 
are important when it comes to environment and package management:

- Environment management (which is mostly concerned with virtual environments)
- Package management
- Python version management
- Package building
- Package publishing

As you can see in the Venn diagram below, lots of tools exist. 

Some can do a single thing (i.e. they are single-purpose), others can 
perform multiple tasks (hence I call them multi-purpose tools).

Links
=======

- :ref:`tuto_python:python_packaging`
