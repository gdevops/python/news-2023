

.. _python_2023_12:

==================================
2023-12
==================================

2023-12-24  The simplest way to build an instant messaging app with Django
=================================================================================

- https://www.photondesigner.com/articles/instant-messenger

I wrote this tutorial to show you the simplest way to add async, real-time events with Django. This includes:

- no heavy dependencies. No Redis. No extra Django channels installation.
- reading from the Django database in real-time 🏎️. We use the new async Django features
- using the lightest available setup (one pip command to install Daphne).

  - Daphne is fully-integrated into Django.
  - Daphne is very easy to deploy in production: 2 lines (see here).

- fast to do ⏰ I want to learn things as fast as possible. You probably do too.



2023-12-24 **GH-113464: A copy-and-patch JIT compiler** by Brandt Bucher
===============================================================================

- https://github.com/brandtbucher
- https://github.com/python/cpython/issues/113464
- https://github.com/python/cpython/pull/113465

::

	'Twas the night before Christmas, when all through the code
	Not a core dev was merging, not even Guido;
	The CI was spun on the PRs with care
	In hopes that green check-markings soon would be there;
	The buildbots were nestled all snug under desks,
	Even PPC64 AIX;
	Doc-writers, triage team, the Council of Steering,
	Had just stashed every change and stopped engineering,

	When in the "PRs" tab arose such a clatter,
	They opened GitHub to see what was the matter.
	Away to CPython they flew like a flash,
	Towards sounds of PROT_EXEC and __builtin___clear_cache.
	First LLVM was downloaded, unzipped
	Then the Actions were running a strange new build script,
	When something appeared, they were stopped in their tracks,
	jit_stencils.h, generated from hacks,
	With their spines all a-shiver, they muttered "Oh, shit...",
	They knew in a moment it must be a JIT.

	More rapid than interpretation it came
	And it copied-and-patched every stencil by name:
	"Now, _LOAD_FAST! Now, _STORE_FAST! _BINARY_OP_ADD_INT!
	On, _GUARD_DORV_VALUES_INST_ATTR_FROM_DICT!
	To the top of the loop! And down into the call!
	Now cache away! Cache away! Cache away all!"
	But why now? And how so? They needed a hint,
	Thankfully, Brandt gave a great talk at the sprint;
	So over to YouTube the reviewers flew,
	They read the white paper, and the blog post too.

	And then, after watching, they saw its appeal
	Not writing the code themselves seemed so unreal.
	And the platform support was almost too easy,
	ARM64 Macs to 32-bit PCs.
	There was some runtime C, not too much, just enough,
	Basically a loader, relocating stuff;
	It ran every test, one by one passed them all,
	With not one runtime dependency to install.
	Mostly build-time Python! With strict static typing!
	For maintenance ease, and also nerd-sniping!

	Though dispatch was faster, the JIT wasn't wise,
	And the traces it used still should be optimized;
	The code it was JIT'ing still needed some thinning,
	With code models small, and some register pinning;
	Or new calling conventions, shared stubs for paths slow,
	Since this JIT was brand new, there was fruit hanging low.
	It was awkwardly large, parsed straight out of the ELFs,
	And they laughed when they saw it, in spite of themselves;

	A configure flag, and no merging this year,
	Soon gave them to know they had nothing to fear;
	It wasn't much faster, at least it could work,
	They knew that'd come later; no one was a jerk,
	But they were still smart, and determined, and skilled,
	They opened a shell, and configured the build;
	--enable-experimental-jit, then made it,
	And away the JIT flew as their "+1"s okay'ed it.
	But they heard it exclaim, as it traced out of sight,
	"Happy JIT-mas to all, and to all a good night!"


Issue: JIT Compilation #113464



2023-12-11 **Announcement: Hatch v1.8.0**
==================================================================================

- https://discuss.python.org/t/announcement-hatch-v1-8-0/40865
- https://babka.social/@pvergain/111573894038324368


I am pleased to announce the release of Hatch v1.8.0 Hatch v1.8.0 - Hatch

This release was a long time in the making and required me slowing down
on development for some time to work on binary distribution techniques.

Now Hatch can be installed easily by any user like most other software.

Special thanks to Ee for getting the certificates set up for the macOS installer!

There is now also a fmt command backed by Ruff with default settings that
I have painstakingly enabled individually to provide as perfect an experience
as possible.

This is the equivalent of what other package managers like Cargo and Go have.

Finally, I’m very excited that Hatch can now manage Python distributions
all by itself which I think users and enterprise IT teams will begin to
prefer over pyenv.

Please let me know what you think
