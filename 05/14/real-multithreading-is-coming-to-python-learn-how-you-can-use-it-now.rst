.. index::
   pair: Python; Real Multithreading
   ! Real Multithreading


.. _multithreading_2023_05_14:

=========================================================================================
2023-05-14 **Real Multithreading is Coming to Python - Learn How You Can Use It Now**
=========================================================================================

- https://martinheinz.dev/blog/97


Python is 32 years old language, yet it still doesn't have proper, true
parallelism/concurrency.

This is going to change soon, thanks to introduction of a "Per-Interpreter
GIL" (Global Interpreter Lock) which will land in Python 3.12.

While release of Python 3.12 is some months away, the code is already
there, so let's take an early peek at how we can use it to write truly
concurrent Python code using **sub-interpreters API**.
